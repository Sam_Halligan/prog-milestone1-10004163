﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
        
            Console.WriteLine("Enter 5 Numbers\n");
            List<double> numbers = new List<double>();
            for (double i = 0; i < 5; i++)
            {

                string userInput;
                double newNumber;
             


                    do
                {
                    Console.Write(string.Format("Enter number #{0}: ", i));
                    userInput = Console.ReadLine();

                  
                }
                while (!double.TryParse(userInput, out newNumber));

                numbers.Add(newNumber);
            }
            Console.WriteLine(string.Join(",", numbers.Select(number => number.ToString())));
            Console.WriteLine(numbers.Sum(str => Convert.ToInt32(str)));







        }
    }
}