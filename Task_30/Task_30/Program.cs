﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 0;
            int y = 0;



            Console.WriteLine("Type two numbers.");

            Console.Write("Number 1=");
            x = int.Parse(Console.ReadLine());

            Console.Write("Number 2=");
            y = int.Parse(Console.ReadLine());

            string strng1 = Convert.ToString(x);
            string strng2 = Convert.ToString(y);

            Console.WriteLine($"Total sum of the 2 numbers in int = {x + y}");

            Console.WriteLine($"Total sum of the 2 numbers in a string = {strng1 + strng2}");
        }
    }
}
