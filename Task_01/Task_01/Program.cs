﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            var age = 0;
            Console.WriteLine("What is your age?");
            age = int.Parse(Console.ReadLine());


            Console.WriteLine("Please Enter Your Name");
            name = Console.ReadLine();
            Console.WriteLine("Hello {0} your age is {1} ", name, age);
            Console.WriteLine("Hello " + name + " your age is " + age + " ");
            Console.WriteLine($"Hello {name} your age is {age}");
            Console.ReadLine();
        }
    }
}
