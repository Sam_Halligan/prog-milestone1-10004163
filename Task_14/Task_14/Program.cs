﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in a Terabyte value to convert it to Gigabytes");
                      
            {
                var getGB = double.Parse(Console.ReadLine());
                var roundedGB = System.Math.Round(getGB * 1024);
                Console.WriteLine($"{getGB}TB is {roundedGB}GB");
            }
        }
    }
}
