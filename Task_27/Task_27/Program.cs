﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                List<string> colours = new List<string>()
              {"Red","Blue","Yellow","Green","Pink"};
                colours.Sort();
                foreach (string c in colours)
                {
                    Console.WriteLine(c);
                }
            }
        }
    }
}