﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] birthdayList = new string[3, 3] { { "Steve is born on the", "19th", "of June" }, { "Joe is born on the", "4th", "of March" }, { "Dave is born on the", "12th", "of August" } };
            
            for (int i = 0; i < birthdayList.GetLength(0); i++)
            {
                for (int j = 0; j < birthdayList.GetLength(1); j++)
                {
                    if (j != birthdayList.GetLength(1))
                    {
                        Console.Write(birthdayList[i, j] + " ");
                    }
                    else
                    {
                        Console.Write(birthdayList[i, j]);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
