﻿
            using System;
            using System.Collections.Generic;
            using System.Linq;
            using System.Text;
            using System.Threading.Tasks;

namespace Task_9
    {
        class Program
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Below are all the leap years for the next 20 years");
                for (int year = 2016; year <= 2036; year++)
                {
                    if (DateTime.IsLeapYear(year))
                    {

                        Console.WriteLine("{0} is a leap year.", year);


                    }
                }
            }
        }
    }

