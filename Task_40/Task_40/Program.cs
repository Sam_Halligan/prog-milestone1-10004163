﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            int days;

            Console.WriteLine("Enter the total amount of days in the month to find out how many Wednesdays there would be if the 1st was a Monday");
            days = int.Parse(Console.ReadLine());

            if (days == 31)
            {
                Console.WriteLine("There are 5 Wednesdays in the month");
            }
            else if (days <= 30)  
            {

                Console.WriteLine("There are 4 Wednesdays in the month");
            }


        }
    }
}
