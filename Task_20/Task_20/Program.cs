﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[7] { 34, 45, 21, 44, 67, 87, 86 };
         
            int[] odd = (from y in array where ((y % 2) == 1) select y).ToArray();
            List<int> oddnumbers = odd.ToList();
            foreach (int o in oddnumbers)
            {
                Console.WriteLine(o);
            }
        }


    }
}
