﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            string month;
            Console.WriteLine("What month were you born in?");
            month = Console.ReadLine();
            var day = 0;
            Console.WriteLine("What day of the month was it?");
            day = int.Parse(Console.ReadLine());
            Console.WriteLine("You were born on day {0} in the month of {1}", day, month);
            Console.ReadLine();
        }
    }
}
