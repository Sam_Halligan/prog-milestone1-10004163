﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
       
            static void Main(string[] args)
        

            {
                Console.Write("Please enter a year to check if it is a leap year:");
                int year = int.Parse(Console.ReadLine());
                if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
                {
                    Console.WriteLine("Yes {0} is a Leap Year", year);
                }
                else
                {
                    Console.WriteLine("No {0} is not a Leap Year", year);

                }
            }
        }
    }
